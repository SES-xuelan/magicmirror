<html>
<head>
    <title>Magic Mirror</title>
    <style type="text/css">
        <?php include('css/main.css') ?>
    </style>
    <link rel="stylesheet" type="text/css" href="css/weather-icons.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <script type="text/javascript">
        var gitHash = '<?php echo trim(`git rev-parse HEAD`) ?>';
    </script>
    <meta name="google" value="notranslate"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <link rel="icon" href="data:;base64,iVBORw0KGgo=">
</head>
<body scroll=no style="overflow:hidden" >

<div class="showimg">
    <img src="image/xiaogongzhu.jpg" style="margin: 0 auto;width: 100%">
</div>
<div class="mainmm">

    <div class="top right">
        <div class="windsun small dimmed"></div>
        <div class="temp"></div>
        <div class="forecast small dimmed"></div>
    </div>
    <div class="top left">
        <div class="date small"></div>
        <div class="time" id="time"></div>
        <div class="calendar xxsmall"></div>
    </div>
    <div class="center-ver center-hor"><!-- <div class="dishwasher light">Vaatwasser is klaar!</div> --></div>
    <div class="center-ver-temp center-hor"></div>
    <div class="lower-third center-hor">
        <div class="compliment light"></div>
    </div>
    <div class="bottom center-hor">
        <div class="temhum medium"></div>
        <div class="webmessage medium"></div>
        <br/>
        <div class="news medium"></div>
    </div>
    
</div>
<!--必须的各种第三方js-->
<script src="js/jquery.js"></script>
<script src="js/jquery.feedToJSON.js"></script>
<script src="js/ical_parser.js"></script>
<script src="js/moment-with-locales.min.js"></script>
<!--各个功能模块-->
<script src="js/paho/mqttws31.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/config.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/rrule.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/version/version.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/calendar/calendar.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/compliments/compliments.js?nocache=<?php echo md5(microtime()) ?>"></script>
<!--<script src="js/weather/openweathermap.js?nocache=--><?php //echo md5(microtime()) ?><!--"></script>-->
<script src="js/weather/seniverse.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/temp_hum/tem_hum.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/time/time.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/news/news.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/update/update.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/webmessage/webmessage.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/stt_handle/stt.js?nocache=<?php echo md5(microtime()) ?>"></script>
<script src="js/main.js?nocache=<?php echo md5(microtime()) ?>"></script>
<!-- <script src="js/socket.io.min.js"></script> -->
<?php include(dirname(__FILE__) . '/controllers/modules.php'); ?>
</body>
</html>
