var version = {
    updateInterval: 60000,
    intervalId: null
}

/**
 * Checks the version and refreshes the page if a new version has been pulled
 */
version.checkVersion = function () {
    $.getJSON('controllers/hash.php')
        .success(function (data) {
            // The githash variable is located in index.php
            if (data && data.gitHash !== gitHash) {
                console.info("gitHash is different");
                window.location.reload();
                window.location.href = window.location.href;
            } else {
                console.info("gitHash is same");
            }
        });

}

version.init = function () {
    this.checkVersion();
    this.intervalId = setInterval(function () {
        this.checkVersion();
    }.bind(this), this.updateInterval);

}
