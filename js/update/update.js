/**
 * 自动更新配置
 * */
var updateConfig = {
    url: config.update.url || null,
    updateInterval: config.update.updateInterval || 600000
}

updateConfig.updateConfigFromUrl = function () {
    $.ajax({
        type: 'GET',
        url: this.url,
        dataType: 'json',
        success: function (data) {
            // alert(data);
            if (data.updateStatus > 0) {
                config = data;
                this.updateConfig();
                console.debug("updateConfigFromUrl success");
            } else {
                console.error('error 1: ' + this.url);
            }

        }.bind(this),
        error: function () {
            // non-specific error message that should be updated
            console.error('error 2: ' + this.url);
        }
    });
}
updateConfig.init = function () {

    if (this.url === null || typeof this.url !== 'string') {
        return false;
    }
    this.updateConfigFromUrl();
    this.intervalId = setInterval(function () {
        this.updateConfigFromUrl();
    }.bind(this), this.updateInterval);

}
//更新配置后调用
updateConfig.updateConfig = function () {
    moment.locale(config.lang);
    calendar.updateConfig();
    compliments.updateConfig();
    news.updateConfig();
    time.updateConfig();
    // openweathermap.updateConfig();
    seniverse.updateConfig();
    tem_hum.updateConfig();
    webmessage.updateConfig();
    stt.updateConfig();
}