var temperature = '0';
var humidity = '0';
var heatIndex = '0';

var socket_temhum = {
    mqttServer: config.tem_hum.mqttServer || 'mqtt.hellowk.cc',
    mqttServerPort: config.tem_hum.mqttServerPort || 9001,
    mqttclientName: config.tem_hum.mqttclientName || "magic_mirror_tem_hum",
    temperatureTopic: config.tem_hum.temperatureTopic || 'homekit/himitsu/temperature',
    humidityTopic: config.tem_hum.humidityTopic || 'homekit/himitsu/humidity',
    heatIndexTopic: config.tem_hum.heatIndexTopic || 'homekit/himitsu/heatIndex',
};

console.log("Connecting to MQTT broker...");
// Create a client instance
client = new Paho.MQTT.Client(socket_temhum.mqttServer, Number(socket_temhum.mqttServerPort), socket_temhum.mqttclientName);
client.connect({
    onSuccess: function () {
        // Once a connection has been made, make a subscription and send a message.
        console.log("MQTT onConnect");
        client.subscribe(socket_temhum.temperatureTopic);
        client.subscribe(socket_temhum.humidityTopic);
        client.subscribe(socket_temhum.heatIndexTopic);
    }
});

client.onConnectionLost = function (responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
    }
};

client.onMessageArrived = function (message) {
    console.log("onMessageArrived:" + message.payloadString);
    var topic = message.destinationName;
    if (topic == 'homekit/himitsu/temperature') {
        temperature = Number(message.payloadString).toFixed(0);
        localStorage.setItem('temperature', temperature);
    } else if (topic == 'homekit/himitsu/humidity') {
        humidity = Number(message.payloadString).toFixed(0);
        localStorage.setItem('humidity', humidity);
    } else if (topic == 'homekit/himitsu/heatIndex') {
        heatIndex = Number(message.payloadString).toFixed(0);
        localStorage.setItem('heatIndex', heatIndex);
    }
};
