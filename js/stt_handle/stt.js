/**
 *
 * */
var stt = {
    showTime: (typeof config.stt == 'undefined') ? 5000 : (config.stt.showTime || 5000),
    updateInterval: 1000,

}

stt.updateMessage = function () {
    $.ajax({
        type: 'GET',
        url: "controllers/stt.php",
        success: function (data) {
            console.info(data);
            if (data.indexOf("1") != -1) {
                stt.showimg();
            }
        }.bind(this),
        error: function () {
            // non-specific error message that should be updated
            console.error('stt error: ' + this.url);
        }
    });
}
stt.init = function () {
    $('.showimg').hide();
    this.updateMessage();
    this.intervalId = setInterval(function () {
        this.updateMessage();
    }.bind(this), this.updateInterval)
}

stt.showimg = function () {
    $('.showimg').fadeIn(this.updateInterval);
    $('.mainmm').fadeOut(1000);
    setTimeout("stt.hideimg()", this.showTime);
}
stt.hideimg = function () {
    $('.showimg').fadeOut(1000);
    $('.mainmm').fadeIn(this.updateInterval);
}

//更新配置后调用
stt.updateConfig = function () {
    this.showTime = (typeof config.stt == 'undefined') ? 5000 : (config.stt.showTime || 5000);
}