var config = {
    lang: 'zh_cn',
    time: {
        timeFormat: 12,
        displaySeconds: true,
        digitFade: false,
    },
    weather: {
        interval: 120000,
        fadeInterval: 10000,
        //open weather map
        openweathermap: {
            params: {
                //city
                q: 'langfang',
                //units: metric or imperial
                units: 'metric',
                // if you want a different lang for the weather that what is set above, change it here
                lang: 'zh_cn',
                APPID: '2b43b471ce8f69b8a373236398070db9'
            }
        },
        //心知天气
        seniverse: {
            apiKey: 'gau3g2jtxmkkvtl7',
            location: 'ip',
            language: 'zh-Hans',
        }

    },
    compliments: {
        interval: 30000,
        fadeInterval: 4000,
        morning: [
            '早上好!',
            '享受新的一天!',
            '昨晚睡得爽歪歪了嘛?'
        ],
        afternoon: [
            '中午好!',
            '你好!'
        ],
        evening: [
            '下班了嘛!',
            '晚上去哪玩儿!',
            '等下去约会嘛!',
            '心情怎么样？',
            '累了吗喝杯茶吧~'
        ],
        weather: {
            //具体解释请看compliments.js中的注释
            '01d': ['今天天气好好啊'],
            '02d': ['今天天气还挺好'],
            '03d': ['今天天气还不错'],
            '04d': ['今天有点风，注意身体'],
            '09d': ['今天会有一点小雨，别忘记带伞'],
            '10d': ['今天有雨，别忘记带伞'],
            '11d': ['今天有大雨，尽量别粗门'],
            '13d': ['哇！下雪了'],
            '50d': ['有点雾，但没啥影响~'],
            '01n': ['今晚天气好好'],
            '02n': ['今晚天气还不错，但是可能见不到月亮了'],
            '03n': ['今晚天气还不错，但是可能见不到月亮了'],
            '04n': ['今晚天气还不错，但是可能见不到月亮了'],
            '09n': ['今晚有点小雨，如果出门的话，别忘记带伞'],
            '10n': ['今晚有雨，如果出门的话，别忘记带伞'],
            '11n': ['今晚有大雨，尽量别粗门 Ｏ(≧▽≦)Ｏ'],
            '13n': ['晚上下雪啦~'],
            '50n': ['今晚天气真糟糕，估计明天会冷']
        }
    },
    calendar: {
        maximumEntries: 3, // Total Maximum Entries
        displaySymbol: true,
        defaultSymbol: 'calendar', // Fontawsome Symbol see http://fontawesome.io/cheatsheet/
        urls: [
            {
                symbol: 'albert-holiday',
                url: 'http://canyoukiss.me/magic_mirror/albert-holiday.ics'
            }
            // {
            // 	symbol: 'soccer-ball-o',
            // 	url: 'https://www.google.com/calendar/ical/akvbisn5iha43idv0ktdalnor4%40group.calendar.google.com/public/basic.ics',
            // },
            // {
            // symbol: 'mars',
            // url: "https://server/url/to/his.ics",
            // },
            // {
            // symbol: 'venus',
            // url: "https://server/url/to/hers.ics",
            // },
            // {
            // symbol: 'venus-mars',
            // url: "https://server/url/to/theirs.ics",
            // },
        ]
    },
    news: {
        feed: [
            'http://www.ftchinese.com/rss/news',
        ]
    },
    update: {
        url: 'http://canyoukiss.me/magic_mirror/update_config.php',
        //更新时间间隔 默认十分钟
        updateInterval: 600000
    },
    webmessage: {
        url: 'http://canyoukiss.me/magic_mirror/webmessage.php',
        //更新时间间隔 默认6秒，updateInterval应该大于showTime
        updateInterval: 6000,
        showTime: 5000
    }
}
