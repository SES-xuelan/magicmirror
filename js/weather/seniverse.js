var seniverse = {
    updateInterval: config.weather.interval || 120000,
    fadeInterval: config.weather.fadeInterval || 10000,
    temperatureLocation: '.temp',
    windSunLocation: '.windsun',
    forecastLocation: '.forecast',
    apiKey: config.weather.seniverse.apiKey || 'APIKEY',
    location: config.weather.seniverse.location || 'ip',
    language: config.weather.seniverse.language || 'zh-Hans',
    apiNow: 'http://canyoukiss.me/magic_mirror/seniverse_weather_now.php',
    apiDaily: 'http://canyoukiss.me/magic_mirror/seniverse_weather_daily.php',
    intervalId: null,
    orientation: 'vertical',
    iconTable: {
        '0':
            '01d',
        '1':
            '01n',
        '2':
            '01d',
        '3':
            '01n',
        '4':
            '03d',
        '5':
            '03d',
        '6':
            '02n',
        '7':
            '03d',
        '8':
            '03n',
        '9':
            '',
        '10':
            '09d',
        '11':
            '09d',
        '12':
            '09d',
        '13':
            'rain',
        '14':
            'rain',
        '15':
            'rain',
        '16':
            'thunderstorm',
        '17':
            'thunderstorm',
        '18':
            'thunderstorm',
        '19':
            '',
        '20':
            '',
        '21':
            'snow',
        '22':
            'snow',
        '23':
            'snow',
        '24':
            'snow',
        '25':
            'snow',
        '26':
            '',
        '27':
            '',
        '28':
            '',
        '29':
            '',
        '30':
            '50d',
        '31':
            '',
        '32':
            '04d',
        '33':
            '',
        '34':
            '',
        '35':
            '',
        '36':
            '',
        '37':
            '',
        '38':
            '',
        '99':
            ''
    }
}

/**
 * Rounds a float to one decimal place
 * @param  {float} temperature The temperature to be rounded
 * @return {float}             The new floating point value
 */
seniverse.roundValue = function (temperature) {
    return parseFloat(temperature).toFixed(1);
}

/**
 * Converts the wind speed (km/h) into the values given by the Beaufort Wind Scale
 * @see http://www.spc.noaa.gov/faq/tornado/beaufort.html
 * @param  {int} kmh The wind speed in Kilometers Per Hour
 * @return {int}     The wind speed converted into its corresponding Beaufort number
 */
seniverse.ms2Beaufort = function (ms) {
    var kmh = ms * 60 * 60 / 1000;
    var speeds = [1, 5, 11, 19, 28, 38, 49, 61, 74, 88, 102, 117, 1000];
    for (var beaufort in speeds) {
        var speed = speeds[beaufort];
        if (speed > kmh) {
            return beaufort;
        }
    }
    return 12;
}

/**
 * Retrieves the current temperature and weather patter from the OpenWeatherMap API
 */
seniverse.updateCurrentWeather = function () {
    var url = seniverse.apiNow + "?location=" + seniverse.location + "&language=" + seniverse.language + "&key=" + seniverse.apiKey;
    $.getJSON(url, function (data) {
        var results = data.results[0];
        var _temperature = results.now.temperature,
            _city = results.location.name;

        var _icon = '<img src="weather_icon/' + results.now.code + '.png"></img>';
        var _newTempHtml = '<span style="font-size:xx-large">'+results.location.name + "</span>&nbsp;" + _icon + '' + _temperature + '&deg;';
        compliments.updateWeather(seniverse.iconTable[results.now.code]);
        $(seniverse.temperatureLocation).updateWithText(_newTempHtml, seniverse.fadeInterval);
    });

}

/**
 * Updates the 5 Day Forecast from the OpenWeatherMap API
 */
seniverse.updateWeatherForecast = function () {
    var url = seniverse.apiDaily + "?location=" + seniverse.location + "&start=0&days=5&language=" + seniverse.language + "&key=" + seniverse.apiKey;
    $.getJSON(url, function (data) {
        var result = data.results[0];
        var _opacity = 1,
            _forecastHtml = '<tr>',
            _forecastHtml2 = '<tr>',
            _forecastHtml3 = '<tr>',
            _forecastHtml4 = '<tr>';

        _forecastHtml = '<table class="forecast-table"><tr>';

        for (var i = 0, count = result.daily.length; i < count; i++) {

            var _forecast = result.daily[i];

            //don't show yesterday's forecast; each date, .dt is 12p local;
            var _12hours = 60 * 60 * 12 * 1000;
            if (_forecast.dt < Math.floor((Date.now() - _12hours) / 1000)) continue;

            if (seniverse.orientation == 'vertical') {
                _forecastHtml2 = '';
                _forecastHtml3 = '';
                _forecastHtml4 = '';
            }

            _forecastHtml += '<td style="opacity:' + _opacity + '" class="day">' + _forecast.date.substr(_forecast.date.length - 5) + '</td>';
            _forecastHtml2 += '<td style="opacity:' + _opacity + '" class="icon-small">' + _forecast.text_day + '</td>';
            _forecastHtml3 += '<td style="opacity:' + _opacity + '" class="temp-max">' + _forecast.high + '℃&nbsp;&nbsp;-</td>';
            _forecastHtml4 += '<td style="opacity:' + _opacity + '" class="temp-min">' + _forecast.low + '℃</td>';

            _opacity -= 0.155;

            if (seniverse.orientation == 'vertical') {
                _forecastHtml += _forecastHtml2 + _forecastHtml3 + _forecastHtml4 + '</tr>';
            }
        }
        _forecastHtml += '</tr>',
            _forecastHtml2 += '</tr>',
            _forecastHtml3 += '</tr>',
            _forecastHtml4 += '</tr>';

        if (seniverse.orientation == 'vertical') {
            _forecastHtml += '</table>';
        } else {
            _forecastHtml += _forecastHtml2 + _forecastHtml3 + _forecastHtml4 + '</table>';
        }

        $(seniverse.forecastLocation).updateWithText(_forecastHtml, seniverse.fadeInterval);
    });
}

seniverse.init = function () {

    this.intervalId = setInterval(function () {
        this.updateCurrentWeather();
        this.updateWeatherForecast();
    }.bind(this), this.updateInterval);
    this.updateCurrentWeather();
    this.updateWeatherForecast();
}

//更新配置后调用
seniverse.updateConfig = function () {
    this.updateInterval = config.weather.interval || 120000;
    this.fadeInterval = config.weather.fadeInterval || 10000;
    this.apiKey = config.weather.seniverse.apiKey || 'APIKEY';
    this.location = config.weather.seniverse.location || 'ip';
    this.language = config.weather.seniverse.language || 'zh-Hans';
}