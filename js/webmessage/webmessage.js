/**
 * 自动更新配置
 * */
var webmessage = {
    url: config.webmessage.url || null,
    updateInterval: config.webmessage.updateInterval || 6000,
    showTime: config.webmessage.showTime || 5000,
    shown: []
}

webmessage.updateMessage = function () {
    //updateInterval应该大于showTime
    if (this.updateInterval <= this.showTime) {
        this.updateInterval = updateInterval + this.showTime;
    }
    $.ajax({
        type: 'GET',
        url: this.url,
        dataType: 'json',
        success: function (data) {
            // alert(data);
            if (data.status > 0) {
                var msgs = data.msgs;
                for (var i = 0; i < msgs.length; i++) {
                    var msg = msgs[i];
                    if (typeof msg == "undefined" || msg == null || msg == "") {
                        console.info("webmessage 空消息");
                    } else if (!this.shown[msg]) {
                        this.shown[msg] = true;
                        $('.webmessage').updateWithText(msg, 500);
                        setTimeout("webmessage.hidemessage()", this.showTime);
                        return;
                    } else {
                        console.info('webmessage 这条信息显示过了 ' + msg);
                    }
                }
            } else {
                console.info('webmessage error 1:' + data);
            }

        }.bind(this),
        error: function () {
            // non-specific error message that should be updated
            console.error('webmessage error 2: ' + this.url);
        }
    });
}
webmessage.init = function () {
    $('.showimg').hide();
    $('.mainmm').show();
    this.updateMessage();
    this.intervalId = setInterval(function () {
        this.updateMessage();
    }.bind(this), this.updateInterval)

}
webmessage.hidemessage = function () {
    $('.webmessage').updateWithText("", 500);
}

webmessage.showimg = function () {
    $('.showimg').fadeIn(3000);
    $('.mainmm').fadeOut(1000);
    setTimeout("webmessage.hideimg()", this.showTime);
}
webmessage.hideimg = function () {
    $('.showimg').fadeOut(3000);
    $('.mainmm').fadeIn(1000);
}

//更新配置后调用
webmessage.updateConfig = function () {
    this.url = config.webmessage.url || null;
    this.updateInterval = config.webmessage.updateInterval || 6000;
    this.showTime = config.webmessage.showTime || 5000;

}