var compliments = {
    complimentLocation: '.compliment',
    currentCompliment: '',
    complimentList: {
        'morning': config.compliments.morning,
        'afternoon': config.compliments.afternoon,
        'evening': config.compliments.evening,
        'weather': config.compliments.weather || {}
    },
    updateInterval: config.compliments.interval || 30000,
    fadeInterval: config.compliments.fadeInterval || 4000,
    intervalId: null,
    currWeather: null
};

/**
 * Changes the compliment visible on the screen
 */
compliments.updateCompliment = function () {


    var _list = [];

    var hour = moment().hour();

    // In the following if statement we use .slice() on the
    // compliments array to make a copy by value.
    // This way the original array of compliments stays intact.

    if (hour >= 3 && hour < 12) {
        // Morning compliments
        _list = compliments.complimentList['morning'].slice();
    } else if (hour >= 12 && hour < 17) {
        // Afternoon compliments
        _list = compliments.complimentList['afternoon'].slice();
    } else if (hour >= 17 || hour < 3) {
        // Evening compliments
        _list = compliments.complimentList['evening'].slice();
    } else {
        // Edge case in case something weird happens
        // This will select a compliment from all times of day
        Object.keys(compliments.complimentList).forEach(function (_curr) {
            _list = _list.concat(compliments.complimentList[_curr]).slice();
        });
    }
    if (this.currWeather != null && this.complimentList.weather != null && this.complimentList.weather[this.currWeather] != null) {
        var _weather_list = [];
        _weather_list = this.complimentList.weather[this.currWeather];
        _weather_list.forEach(function (itm) {
            _list.push(itm);
        });
    }

    // Search for the location of the current compliment in the list
    var _spliceIndex = _list.indexOf(compliments.currentCompliment);

    // If it exists, remove it so we don't see it again
    if (_spliceIndex !== -1) {
        _list.splice(_spliceIndex, 1);
    }

    // Randomly select a location
    var _randomIndex = Math.floor(Math.random() * _list.length);
    compliments.currentCompliment = _list[_randomIndex];

    $('.compliment').updateWithText(compliments.currentCompliment, compliments.fadeInterval);

}
//根据天气更换问候语
compliments.updateWeather = function (weatherCode) {
    //  '01d':'day-sunny',
    //  '02d':'day-cloudy',
    //  '03d':'cloudy',
    //  '04d':'cloudy-windy',
    //  '09d':'showers',
    //  '10d':'rain',
    //  '11d':'thunderstorm',
    //  '13d':'snow',
    //  '50d':'fog',
    //  '01n':'night-clear',
    //  '02n':'night-cloudy',
    //  '03n':'night-cloudy',
    //  '04n':'night-cloudy',
    //  '09n':'night-showers',
    //  '10n':'night-rain',
    //  '11n':'night-thunderstorm',
    //  '13n':'night-snow',
    //  '50n':'night-alt-cloudy-windy'
    console.debug("updateWeather=>" + weatherCode);
    this.currWeather = weatherCode;

}

compliments.init = function () {

    this.updateCompliment();

    this.intervalId = setInterval(function () {
        this.updateCompliment();
    }.bind(this), this.updateInterval)

}

//更新配置后调用
compliments.updateConfig = function () {
    this.complimentList = {
        'morning': config.compliments.morning,
        'afternoon': config.compliments.afternoon,
        'evening': config.compliments.evening,
        'weather': config.compliments.weather || {}
    };
    this.updateInterval = config.compliments.interval || 30000;
    this.fadeInterval = config.compliments.fadeInterval || 4000;
    this.updateCompliment();
}
