<?php
header("Access-Control-Allow-Origin: *");
header('Content-Type:application/json');//这个类型声明非常关键

$config = array(
    'updateStatus' => 200,
    'lang' => 'zh_cn',
    'time' => array(
        'timeFormat' => 24,
        'displaySeconds' => true,
        'digitFade' => false,
    ),
    'weather' => array(
        'interval' => 120000,
        'fadeInterval' => 10000,
        'openweathermap' => array(
            'params' => array(
                'q' => 'langfang',
                'units' => 'metric',
                'lang' => 'zh_cn',
                'APPID' => '2b43b471ce8f69b8a373236398070db9'//2b43b471ce8f69b8a373236398070db9  ca0c42f91616dfbb1ddbe3ece886ecc1
            )
        ),
        'seniverse' => array(
            'apiKey' => 'gau3g2jtxmkkvtl7',
            'location' => 'ip',
            'language' => 'zh-Hans',
        )

    ),
    'compliments' => array(
        'interval' => 30000,
        'fadeInterval' => 4000,
        'morning' => array(
//            '小公主早上好',
//            '小公主，耐你',
//            '昨晚睡得咋样?小公主'
            '早上的提示1',
            '早上的提示2',
            '早上的提示3',
            '早上的提示4'
        ),
        'afternoon' => array(
            '下午的提示1',
            '下午的提示2',
            '下午的提示3',
            '下午的提示4'
        ),
        'evening' => array(
            '晚上的提示1',
            '晚上的提示2'
        ),
        'weather' => array(
            //  '01d':'day-sunny',
            //  '02d':'day-cloudy',
            //  '03d':'cloudy',
            //  '04d':'cloudy-windy',
            //  '09d':'showers',
            //  '10d':'rain',
            //  '11d':'thunderstorm',
            //  '13d':'snow',
            //  '50d':'fog',
            //  '01n':'night-clear',
            //  '02n':'night-cloudy',
            //  '03n':'night-cloudy',
            //  '04n':'night-cloudy',
            //  '09n':'night-showers',
            //  '10n':'night-rain',
            //  '11n':'night-thunderstorm',
            //  '13n':'night-snow',
            //  '50n':'night-alt-cloudy-windy'
            '01d' => array('今天天气好好啊'),
            '02d' => array('今天天气还挺好'),
            '03d' => array('今天天气还不错'),
            '04d' => array('今天有点风，注意身体'),
            '09d' => array('今天会有一点小雨，别忘记带伞'),
            '10d' => array('今天有雨，别忘记带伞'),
            '11d' => array('今天有大雨，尽量别粗门'),
            '13d' => array('哇！下雪了'),
            '50d' => array('有点雾，但没啥影响~'),
            '01n' => array('今晚天气好好'),
            '02n' => array('今晚天气还不错，但是可能见不到月亮了'),
            '03n' => array('今晚天气还不错，但是可能见不到月亮了'),
            '04n' => array('今晚天气还不错，但是可能见不到月亮了'),
            '09n' => array('今晚有点小雨，如果出门的话，别忘记带伞'),
            '10n' => array('今晚有雨，如果出门的话，别忘记带伞'),
            '11n' => array('今晚有大雨，尽量别粗门 Ｏ(≧▽≦)Ｏ'),
            '13n' => array('晚上下雪啦~'),
            '50n' => array('今晚天气真糟糕，估计明天会冷')
        )
    ),
    'calendar' => array(
        'maximumEntries' => 3,
        'displaySymbol' => true,
        'defaultSymbol' => 'calendar',
        'urls' => array(
            array(
                'symbol' => 'albert-holiday',
                'url' => 'http://canyoukiss.me/magic_mirror/albert-holiday.ics'
            )
        )
    ),
    'news' => array(
        'feed' => array('http://www.ftchinese.com/rss/news')
    ),
    'update' => array(
        'url' => 'http://canyoukiss.me/magic_mirror/update_config.php',
        'updateInterval' => 600000
    ),
    'webmessage' => array(
        'url' => 'http://canyoukiss.me/magic_mirror/webmessage.php',
        'updateInterval' => 6000,
        'showTime' => 5000
    )
);

echo json_encode($config);

?>