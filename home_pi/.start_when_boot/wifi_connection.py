#!/usr/bin/env python
# coding=utf-8
# author:albert

import os
import time

while True:
    time.sleep(10)
    # 扫描wifi
    scan_dict = {}
    scan_wifis = os.popen("iwlist wlan0 scan | sort | uniq |grep 'ESSID:' | awk '{print $1}'").readlines()
    for i in scan_wifis:
        enc_ssid = i[6:].replace("\n", "").strip('"').decode("string_escape")
        if enc_ssid[0:3] == "MM+":
            ssid = enc_ssid[3:]
            pos = ssid.index("+")
            if pos > 0:
                scan_dict[ssid[0:pos]] = ssid[pos + 1:]

    #print "scan_dict=>"
    #print scan_dict
    # 已有wifi
    local_dict = {}
    local_wifis = os.popen("sudo cat /etc/wpa_supplicant/wpa_supplicant.conf | grep 'ssid=' | awk '{print $1}'").readlines()
    for i in local_wifis:
        wifi = i[5:].replace("\n", "").strip('"').decode("string_escape")
        local_dict[wifi] = 1

    wpa_supplicant = os.popen("sudo cat /etc/wpa_supplicant/wpa_supplicant.conf").read()
    #print "wpa_supplicant=>"
    #print wpa_supplicant
    wpa_supplicant = wpa_supplicant.replace("\"", "\\\"")
    # 处理数据
    should_reboot = False
    for ssid in scan_dict:
        if not local_dict.has_key(ssid):
            should_reboot = True
            print u"\tssid=>%s\n\tpwd=>%s\n已添加！" % (ssid, scan_dict[ssid])
            str = "\nnetwork={\n" \
                  "    ssid=\\\"%s\\\"\n" \
                  "    psk=\\\"%s\\\"\n" \
                  "    key_mgmt=WPA-PSK\n" \
                  "    priority=1\n" \
                  "}\n" % (ssid, scan_dict[ssid])
            wpa_supplicant = wpa_supplicant + str

    if should_reboot:
        wpa_supplicant = os.popen(
            "sudo sh -c 'echo \"%s\" > /etc/wpa_supplicant/wpa_supplicant.conf'" % wpa_supplicant).read()
        wpa_supplicant = os.popen("echo 'test' >/home/pi/wificonnection.test").read()
        wpa_supplicant = os.popen("sudo reboot").read()
        #print "Reboot!!"
    #os.popen("echo `date`>/home/pi/wifitest.txt")
    #print "while end"
