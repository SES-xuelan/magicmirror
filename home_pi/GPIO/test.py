#!/usr/bin/env python
# coding=utf-8
# author:albert

import RPi.GPIO as GPIO
import time
import os, sys
import signal

GPIO.setmode(GPIO.BCM)

# define GPIO pin
pin_led = 14

GPIO.setup(pin_led, GPIO.OUT, initial=GPIO.LOW)


def cleanup():
    # 释放资源，不然下次运行是可能会收到警告
    print('clean up')
    GPIO.cleanup()


try:
    while True:
        GPIO.output(pin_led, 1)
        time.sleep(0.1)
        # print str(press_time)
except KeyboardInterrupt:
    print('User press Ctrl+c ,exit;')
finally:
    cleanup()
