#!/usr/bin/env python
# coding=utf-8
# author:albert

import RPi.GPIO as GPIO
import time
import os, sys
import signal

GPIO.setmode(GPIO.BCM)

# define GPIO pin
pin_btn = 17
pin_led = 26

GPIO.setup(pin_btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pin_led, GPIO.OUT, initial=GPIO.LOW)
led_on = 1
press_time = 0


def cleanup():
    # 释放资源，不然下次运行是可能会收到警告
    print('clean up')
    GPIO.cleanup()


try:
    while True:
        if press_time >= 30:
            print 'shutdown! '
            press_time = 0
            GPIO.output(pin_led, 0)
            os.system("sudo shutdown -h now")
        if GPIO.input(pin_btn) == 1:
            GPIO.output(pin_led, 1)
            press_time += 1
        else:
            GPIO.output(pin_led, 0)
            press_time = 0
        time.sleep(0.1)
        # print str(press_time)
except KeyboardInterrupt:
    print('User press Ctrl+c ,exit;')
finally:
    cleanup()
