# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

alias cls='clear'
alias shutDown='sudo shutdown -h now'
alias reboot='sudo reboot'
export LANG="en_GB.UTF-8"
export LC_ALL="en_GB.UTF-8"

count=`ps -ef |grep chromium-browser |grep -v "grep"|wc -l`
if [ 0 == $count ];then
    #echo 'a'
    export DISPLAY=:0.0 && chromium-browser --incognito --disable-popup-blocking --no-first-run --disable-desktop-notifications --kiosk "http://127.0.0.1/" &
    
fi

count_wifi=`ps -ef |grep '/home/pi/.start_when_boot/wifi_connection.py' |grep -v "grep"|wc -l`
if [ 0 == $count_wifi ];then
    #echo '12333'
    python /home/pi/.start_when_boot/wifi_connection.py &
fi

alias findChrom='ps aux |grep chromium-browser'

